const os = require('os')
const user = os.userInfo()

console.log(user)

console.log(`the system has been up for ${os.uptime()} seconds`)

const sinfo = {
    name: os.platform(),
    type: os.type(),
    release: os.release(),
    version: os.version(),
    totMem: os.totalmem(),
    freeMem: os.freemem(),
}
console.log(sinfo)